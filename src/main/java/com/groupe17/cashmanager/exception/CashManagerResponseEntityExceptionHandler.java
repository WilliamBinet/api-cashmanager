package com.groupe17.cashmanager.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

@ControllerAdvice
@RestController
public class CashManagerResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseException> handleAllExceptions(Exception e, WebRequest webRequest){
        ResponseException responseException = new ResponseException(new Date(), e.getMessage(), webRequest.getDescription(false));
        return new ResponseEntity<>(responseException, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ProductNotFoundException.class)
    public ResponseEntity<ResponseException> handleUserNotFount(ProductNotFoundException e, WebRequest webRequest){
        ResponseException responseException = new ResponseException(new Date(), e.getMessage(), webRequest.getDescription(false));
        return new ResponseEntity<>(responseException, HttpStatus.NOT_FOUND);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ResponseException responseException = new ResponseException(new Date(), ex.getMessage(), ex.getBindingResult().toString());
        return new ResponseEntity<>(responseException, HttpStatus.BAD_REQUEST);
    }
}
