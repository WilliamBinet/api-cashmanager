package com.groupe17.cashmanager.exception;

public class InvalidChequeException extends RuntimeException {
    public InvalidChequeException(String chequeInvalideMessage) {
        super(chequeInvalideMessage);
    }
}
