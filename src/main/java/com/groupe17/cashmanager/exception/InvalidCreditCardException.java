package com.groupe17.cashmanager.exception;


public class InvalidCreditCardException extends RuntimeException {

    public InvalidCreditCardException(String message) {
        super(message);
    }
}
