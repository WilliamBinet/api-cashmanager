package com.groupe17.cashmanager.configuration;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {
    private static final Contact DEFAUT_CONTACT = new Contact("","","");
    private static final ApiInfo DEFAULT_API_INFO = new ApiInfo("CashManager API Documentation", "Swagger documentation", "1.0", "urn:tos", DEFAUT_CONTACT.getName() ,"Apache 2.0", "http://apache.org/");
    private static final Set<String> DEFAULT_CONSUME_PRODUCES = new HashSet<String>(Collections.singletonList("application/json"));

    /***
     * Configure the generation of the Swagger
     * @return Docket for Swagger
     */
    @Bean
    public Docket api(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(DEFAULT_API_INFO)
                .produces(DEFAULT_CONSUME_PRODUCES).consumes(DEFAULT_CONSUME_PRODUCES).select()
                .apis(RequestHandlerSelectors.basePackage("com.groupe17.cashmanager")).build();
    }
}
