package com.groupe17.cashmanager.configuration;

import com.groupe17.cashmanager.DTO.payment.paymentMethod.Cheque;
import com.groupe17.cashmanager.DTO.payment.paymentMethod.CreditCard;
import com.groupe17.cashmanager.routes.Authentication;
import com.groupe17.cashmanager.routes.CartRoutes;
import com.groupe17.cashmanager.routes.PaymentRoutes;
import com.groupe17.cashmanager.routes.ProductRoutes;
import org.springframework.hateoas.Link;

import java.util.HashMap;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class ControllerLinks {

    /***
     * Get the add to cart link for hateoas usage
     * @param idCart Id of the cart
     * @return Link
     */
    public static Link getAddToCartLink(Long idCart) {
        return linkTo(methodOn(CartRoutes.class).addToCart(idCart, new HashMap<>(), null))
                .withRel(Constants.HATEOAS_ADD_TO_CART);
    }

    /***
     * Get the cart link for hateoas usage
     * @param idCart Id of the cart
     * @return Link
     */
    public static Link getCurrentLinkToCart(Long idCart){
        return linkTo(methodOn(CartRoutes.class).findOne(idCart))
                .withRel(Constants.HATEOAS_SELF);
    }


    /***
     * Get the remove to cart link for hateoas usage
     * @param idCart Id of the cart
     * @return Link
     */
    public static Link getRemoveFromCartLink(Long idCart) {
        return linkTo(methodOn(CartRoutes.class).removeFromCart(idCart, new HashMap<>()))
                .withRel(Constants.HATEOAS_REMOVE_TO_CART);
    }

    /***
     * Get the pay cart with credit card link for hateoas usage
     * @param idCart Id of the cart
     * @return Link
     */
    public static Link getPayCartWithCB(Long idCart) {
        return linkTo(methodOn(PaymentRoutes.class).pay(new CreditCard("",null,""),idCart,null))
                .withRel(Constants.HATEOAS_PAY_WITH_CB);
    }


    /***
     * Get the pay cart with cheque link for hateoas usage
     * @param idCart Id of the cart
     * @return Link
     */
    public static Link getPayCartWithCheque(Long idCart) {
        return linkTo(methodOn(PaymentRoutes.class).pay(new Cheque(0D,"",""),idCart,null)).withRel(Constants.HATEOAS_PAY_WITH_CHEQUE);
    }

    /***
     * Get the authentication route link
     * @return Link
     */
    public static Link getAuthenticationRoute() {
        return linkTo(methodOn(Authentication.class).createNewClient(new HashMap<>())).withRel(Constants.HATEOAS_AUTH);
    }

    /***
     * Get the all products route link
     * @return Link
     */
    public static Link getAllProduct() {
        return linkTo(methodOn(ProductRoutes.class).findAll()).withRel("All Products");
    }

    public static Link getCleanCart(long id) {
        return linkTo(methodOn(CartRoutes.class).cleanCart(id))
                .withRel(Constants.HATEOAS_REMOVE_ALL_PRODUCT);
    }

    public static Link getCancelPayment(long id) {
        return linkTo(methodOn(PaymentRoutes.class).cancelPayment(id, null)).withRel(Constants.HATEOAS_CANCELED_PAYMENT);
    }
}
