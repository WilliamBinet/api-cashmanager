package com.groupe17.cashmanager.configuration;


public class Constants {
    public static final Integer MAX_ATTEMPT_CB = 3;
    public static final Integer MAX_ATTEMPT_CHEQUE = 3;
    public static final String MESSAGE_ERREUR_INVALID_CHEQUE = "Le cheque est invalide";
    public static final String MESSAGE_ERREUR_INVALID_CARD = "La carte est invalide";
    public static final String MESSAGE_ERREUR_MAX_ATTEMPT_PAYMENT = "Vous avez trop d'essai avec ce moyen de paiement";
    public static final String MESSAGE_ERREUR_PRODUCT_NOT_FOUND = "Product not found";
    public static final String MESSAGE_SUCCESS_CARD = "Paiement par carte accepté";
    public static final String HATEOAS_CANCELED_PAYMENT = "Cancel the payment of the cart";
    public static final String MESSAGE_SUCCESS_CHEQUE = "Paiement par cheque accepté";
    public static final String HATEOAS_ADD_TO_CART = "Add a product to the cart";
    public static final String HATEOAS_REMOVE_ALL_PRODUCT = "Remove all products from the cart";
    public static final String HATEOAS_REMOVE_TO_CART = "Remove a product from the cart";
    public static final String HATEOAS_PAY_WITH_CB = "Pay the cart with a Credit card";
    public static final String HATEOAS_PAY_WITH_CHEQUE = "Pay the cart with a Cheque";
    public static final String HATEOAS_SELF = "self";
    public static final String HATEOAS_AUTH = "New cart";
    public static final String MESSAGE_ERROR_WRONG_PASSWORD = "The 'password' parameter must not be null or empty";
}
