package com.groupe17.cashmanager.configuration;

import com.groupe17.cashmanager.DTO.Cart;
import com.groupe17.cashmanager.DTO.Terminal;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import java.util.Base64;

public class JWTToken {

    private String token;
    private static final byte[] secret = Base64.getDecoder().decode("QCCNsow2FKxpALfJXZ9XSPx6BEqzftQAGNfWCobzTN8=");


    public JWTToken(Terminal terminal) {

       this.token = Jwts.builder()
                .claim("Terminal_ID", terminal.getId())
               .claim("Cart_Id", terminal.getCart().getId())
                .signWith(Keys.hmacShaKeyFor(secret))
                .compact();

        System.out.println(this.token);
    }

    /***
     * Check if the cart Id in a jwt token is the same a the one pass to the parameter
     * @param jwtString JWT Token
     * @param cartId   Id of the Cart
     * @return boolean
     */
    public static boolean isSameCart(String jwtString, Long cartId) {
        Jws<Claims> result = JWTToken.getJwsResult(jwtString);
        Integer  cartIdInt = (Integer) result.getBody().get("Cart_Id");
        Long idJwt = Long.valueOf(cartIdInt) ;
        return idJwt.equals(cartId);
    }

    /***
     * getter token
     * @return token
     */
    public String getToken() {
        return token;
    }

    /***
     * Verify if jwtString is a valid JWT Token
     * @param jwtString Jwt String
     * @return boolean
     */
    public static Boolean isJWTValid(String jwtString){
        Jws<Claims> result = JWTToken.getJwsResult(jwtString);
        return Terminal.findTerminalById((String) result.getBody().get("Terminal_ID")) != null;
    }

    /***
     * Return in a understandable way a JWT
     * @param jwtString The JWT to read
     * @return Jws Claims (Readable jwt)
     */
    public static Jws<Claims> getJwsResult(String jwtString){
        return Jwts.parser()
                .setAllowedClockSkewSeconds(62)
                .setSigningKey(Keys.hmacShaKeyFor(secret))
                .parseClaimsJws(jwtString);
    }


    /***
     * Retrieve the terminal linked to the jwtString
     * @param jwtString The JWT to read
     * @return The Linked Terminal
     */
    public static Terminal getTerminal (String jwtString) {
        Jws<Claims> result = JWTToken.getJwsResult(jwtString);
        return Terminal.findTerminalById((String) result.getBody().get("Terminal_ID"));
    }
}
