package com.groupe17.cashmanager.DAO.JpaRepositories;

import com.groupe17.cashmanager.DTO.Cart;
import org.springframework.data.jpa.repository.JpaRepository;

/***
 * Database action on the Cart entity
 */
public interface CartRespository extends JpaRepository<Cart, Long> {


}
