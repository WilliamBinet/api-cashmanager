package com.groupe17.cashmanager.DAO.JpaRepositories;

import com.groupe17.cashmanager.DTO.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/***
 * Database action on the Product entity
 */

public interface ProductRespository extends JpaRepository<Product, Long> {
    Optional<Product> findProductByReference(String reference);
}