package com.groupe17.cashmanager.routes;

import com.groupe17.cashmanager.DAO.JpaRepositories.CartRespository;
import com.groupe17.cashmanager.DTO.Cart;
import com.groupe17.cashmanager.DTO.Terminal;
import com.groupe17.cashmanager.configuration.Constants;
import com.groupe17.cashmanager.configuration.ControllerLinks;
import org.springframework.hateoas.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.HashMap;
import java.util.Map;

@RestController
public class Authentication {

    private final
    CartRespository cartRespository;

    public Authentication(CartRespository cartRespository) {
        this.cartRespository = cartRespository;
    }

    /***
     * Route to generate a new cart and return the JWT token linked to the Terminal used with this cart
     * @param password Password to be able to create a new Cart
     * @return Terminal
     */
    @PostMapping("/authentication")
    public Resource<Map<String, String>>  createNewClient(@RequestBody Map<String, String> password){
        if (password.containsKey("password") && password.get("password").equals("password")) {
            Cart cart = new Cart();
            Cart newCart =  cartRespository.save(cart);
            Terminal terminal = new Terminal(newCart);
            HashMap<String,String> responseMap = new HashMap<>();
            responseMap.put("terminal", terminal.getJWT_Token());
            Resource<Map<String, String>> resource = new Resource<>(responseMap);
            resource.add(ControllerLinks.getAddToCartLink(cart.getId()));
            resource.add(ControllerLinks.getRemoveFromCartLink(cart.getId()));
            resource.add(ControllerLinks.getPayCartWithCB(cart.getId()));
            resource.add(ControllerLinks.getPayCartWithCheque(cart.getId()));
            resource.add(ControllerLinks.getCleanCart(cart.getId()));
            resource.add(ControllerLinks.getAllProduct());
            return resource;
        } else {
            throw new IllegalArgumentException(Constants.MESSAGE_ERROR_WRONG_PASSWORD);
        }
    }



}
