package com.groupe17.cashmanager.routes;

import com.groupe17.cashmanager.DAO.JpaRepositories.CartRespository;
import com.groupe17.cashmanager.DTO.Cart;
import com.groupe17.cashmanager.DTO.Terminal;
import com.groupe17.cashmanager.DTO.payment.PayCart;
import com.groupe17.cashmanager.DTO.payment.paymentMethod.Cheque;
import com.groupe17.cashmanager.DTO.payment.paymentMethod.CreditCard;
import com.groupe17.cashmanager.DTO.payment.PaymentUtils;
import com.groupe17.cashmanager.DTO.payment.paymentMethod.PaymentState;
import com.groupe17.cashmanager.configuration.Constants;
import com.groupe17.cashmanager.configuration.ControllerLinks;
import com.groupe17.cashmanager.configuration.JWTToken;
import com.groupe17.cashmanager.exception.CartNotFoundExeption;
import com.groupe17.cashmanager.exception.InvalidChequeException;
import org.springframework.hateoas.Resource;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class PaymentRoutes {

    private final CartRespository cartRespository;

    public PaymentRoutes(CartRespository cartRespository) {
        this.cartRespository = cartRespository;
    }

    /***
     * Pay the cart with a credit card
     * @param creditCard Credit Card with witch the cart will be paid
     * @param idCart Cart to pay
     * @param token JWT Token of the Terminal linked to the cart
     * @return PaymentState
     */
    @PostMapping("/pay/creditCard/{idCart}")
    public Resource<PaymentState> pay(@Valid @RequestBody CreditCard creditCard, @PathVariable long idCart, @RequestHeader("Authorization") String token) {
        if (JWTToken.isJWTValid(token) && JWTToken.isSameCart(token, idCart)) {
            Terminal terminal = Terminal.findTerminalViaJWTToken(token);
            Cart cart = terminal.getCart();
            if (cart.getPaymentMethod() != null && cart.getPaymentMethod().getClass() == CreditCard.class) {
                CreditCard initialCreditCart = (CreditCard) cart.getPaymentMethod();
                if (initialCreditCart.equals(creditCard)) {
                    creditCard = initialCreditCart;
                }
            }

            cart.setPaymentMethod(creditCard);
            Resource<PaymentState> resource = new Resource<>(PayCart.pay(cart).paymentState);

            if (resource.getContent() == PaymentState.ACCEPTED) {
                resource.add(ControllerLinks.getAuthenticationRoute());
            } else {
                resource.add(ControllerLinks.getPayCartWithCB(cart.getId()));
                resource.add(ControllerLinks.getPayCartWithCheque(cart.getId()));
            }
            return resource;
        } else {
            throw new IllegalArgumentException("JWT Invalid");
        }
    }

    /***
     * Pay the carte using a cheque
     * @param cheque Cheque with which the card will be paid
     * @param idCart Cart to pay
     * @param token JWT Token of the terminal link to the cart
     * @return PaymentState
     */
    @PostMapping("/pay/cheque/{idCart}")
    public Resource<PaymentState> pay(@Valid @RequestBody Cheque cheque, @PathVariable long idCart , @RequestHeader("Authorization") String token) {
       if (this.isSameCartJWT(idCart, token)) {
           Optional<Cart> cart = cartRespository.findById(idCart);
           if (!cart.isPresent()) {
               throw new CartNotFoundExeption(String.valueOf(idCart));
           }
           if (PaymentUtils.isChequeValid(cheque)) {
               cart.get().setPaymentMethod(cheque);
               PaymentState state = PayCart.pay(cart.get()).paymentState;
               Resource<PaymentState> resource = new Resource<>(state);
               if (state == PaymentState.ACCEPTED) {
                   resource.add(ControllerLinks.getAuthenticationRoute());
               } else {
                   resource.add(ControllerLinks.getPayCartWithCB(cart.get().getId()));
                   resource.add(ControllerLinks.getPayCartWithCheque(cart.get().getId()));
               }
               return resource;
           } else {
               throw new InvalidChequeException(Constants.MESSAGE_ERREUR_INVALID_CHEQUE);
           }
       } else {
           throw new IllegalArgumentException("JWT Invalid");
       }
    }

    /***
     * Cancel the payment of the cart
     * @param idCart Cart to cancel
     * @param token Jwt token linked to the cart to cancel
     * @return PaymentState
     */
    @PostMapping("/pay/cancel/{idCart}")
    public Resource<PaymentState> cancelPayment(@PathVariable long idCart, @RequestHeader("Authorization") String token) {
        if (this.isSameCartJWT(idCart, token)) {
            Optional<Cart> cart = cartRespository.findById(idCart);
            if (!cart.isPresent()) {
                throw new CartNotFoundExeption(String.valueOf(idCart));
            }
            return new Resource<>(PayCart.cancelPayment(cart.get()).paymentState);
        } else {
            throw new IllegalArgumentException("JWT Invalid");

        }
    }

    /***
     * check if the JWT token cart match with  idCart
     * @param idCart CartId To compare
     * @param token Token to compare
     * @return Boolean
     */
    private Boolean isSameCartJWT(long idCart, String token) {
        return JWTToken.isJWTValid(token) && JWTToken.isSameCart(token, idCart);
    }
}