package com.groupe17.cashmanager.routes;

import com.groupe17.cashmanager.DAO.JpaRepositories.CartRespository;
import com.groupe17.cashmanager.DAO.JpaRepositories.ProductRespository;
import com.groupe17.cashmanager.DTO.Cart;
import com.groupe17.cashmanager.DTO.Product;
import com.groupe17.cashmanager.DTO.plurals.Carts;
import com.groupe17.cashmanager.configuration.ControllerLinks;
import com.groupe17.cashmanager.configuration.JWTToken;
import com.groupe17.cashmanager.exception.CartNotFoundExeption;
import com.groupe17.cashmanager.exception.ProductNotFoundException;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Map;
import java.util.Optional;

@RestController
@Api(tags = "Carts")
public class CartRoutes {

    @Autowired
    private CartRespository cartRespository;

    @Autowired
    private ProductRespository productRespository;


    /***
     * Route to get All the cart
     * @return Resources of cart
     */
    @Transactional
    @GetMapping("/carts")
    public Resources<Resource<Cart>> findAll() {
        Carts carts = new Carts(cartRespository.findAll());
        Resources<Resource<Cart>> resources = new Resources<>(carts.toResources());
        return resources;
    }

    @GetMapping("/carts/{id}")
    @ResponseBody
    public Resource<Cart> findOne(@PathVariable long id) {
        Optional<Cart> cart = cartRespository.findById(id);
        if (!cart.isPresent()) {
            throw new CartNotFoundExeption(String.valueOf(id));
        }
        Resource<Cart> resource = new Resource<>(cart.get());
        resource.add(ControllerLinks.getCurrentLinkToCart(cart.get().getId()));
        resource.add(ControllerLinks.getAddToCartLink(cart.get().getId()));
        resource.add(ControllerLinks.getRemoveFromCartLink(cart.get().getId()));
        resource.add(ControllerLinks.getPayCartWithCB(cart.get().getId()));
        resource.add(ControllerLinks.getPayCartWithCheque(cart.get().getId()));
        return resource;
    }


    /***
     * Add a product to the cart
     * @param id Id of the cart to add the product
     * @param reference Reference of the product to Add
     * @param token JWT Token of the linked Terminal
     * @return Response Entity Resource Cart
     */
    @PostMapping("/carts/{id}/add")
    public ResponseEntity<Resource<Cart>> addToCart(@PathVariable long id, @RequestBody Map<String, String> reference, @RequestHeader("Authorization") String token) {
        if ( JWTToken.isJWTValid(token) && JWTToken.isSameCart(token, id)) {
            System.out.println("token = " + token);
        } else {
            throw new IllegalArgumentException("JWT Invalid");
        }
        if (reference.containsKey("reference")) {
            Optional<Cart> cart = cartRespository.findById(id);
            Optional<Product> product = productRespository.findProductByReference(reference.get("reference"));
            if (!cart.isPresent()) {
                throw new CartNotFoundExeption(String.valueOf(id));
            } else if (!product.isPresent()) {
                throw new ProductNotFoundException("Aucun produit avec la réference: " + reference);
            }
            cart.get().addProduct(product.get());
            Cart newCart = cartRespository.save(cart.get());
            URI location = ServletUriComponentsBuilder
                    .fromCurrentRequest().path("/{id}").buildAndExpand(newCart.getId()).toUri();
            Resource<Cart> cartResource = getAllLinkHateosForCart(newCart);
            return ResponseEntity.created(location).body(cartResource);
        }
        throw new IllegalArgumentException("The 'reference' parameter must not be null or empty");
    }

    /***
     * Remove the product of the cart
     * @param id Id of the cart
     * @param reference Reference of the product to remove
     * @return Resource Cart
     */
    @PostMapping("/carts/{id}/remove")
    public ResponseEntity<Resource<Cart>> removeFromCart(@PathVariable long id, @RequestBody Map<String, String> reference) {
        if (reference.containsKey("reference")) {
            Optional<Cart> cart = cartRespository.findById(id);
            Optional<Product> product = productRespository.findProductByReference(reference.get("reference"));
            if (!cart.isPresent()) {
                throw new CartNotFoundExeption(String.valueOf(id));
            } else if (!product.isPresent()) {
                throw new ProductNotFoundException("Aucun produit avec la réference: " + reference);
            }
            cart.get().removeProduct(product.get());
            cartRespository.save(cart.get());
            Resource<Cart> cartResource = getAllLinkHateosForCart(cart.get());
            return ResponseEntity.accepted().body(cartResource);
        } else {
            throw new IllegalArgumentException("The 'reference' parameter must not be null or empty");
        }
    }


    @PostMapping("/carts/{id}/clean")
    public ResponseEntity<Resource<Cart>> cleanCart(@PathVariable long id) {
        Optional<Cart> cart = cartRespository.findById(id);
        if (!cart.isPresent()) {
            throw new CartNotFoundExeption(String.valueOf(id));
        }
        cart.get().clearAllProduct();
        cartRespository.save(cart.get());
        Resource<Cart> cartResource = getAllLinkHateosForCart(cart.get());
        return ResponseEntity.accepted().body(cartResource);
    }

    /***
     * Get All link Hateoas for the Cart entity
     * @param cart Card to get link
     * @return Resource cart
     */
    private Resource<Cart> getAllLinkHateosForCart(Cart cart) {
        Resource<Cart> cartResource = new Resource<>(cart);
        cartResource.add(ControllerLinks.getCurrentLinkToCart(cart.getId()));
        cartResource.add(ControllerLinks.getAddToCartLink(cart.getId()));
        cartResource.add(ControllerLinks.getRemoveFromCartLink(cart.getId()));
        cartResource.add(ControllerLinks.getPayCartWithCB(cart.getId()));
        cartResource.add(ControllerLinks.getPayCartWithCheque(cart.getId()));
        cartResource.add(ControllerLinks.getCleanCart(cart.getId()));
        cartResource.add(ControllerLinks.getCancelPayment(cart.getId()));

        return cartResource;
    }
}
