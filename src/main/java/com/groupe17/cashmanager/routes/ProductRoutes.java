package com.groupe17.cashmanager.routes;

import com.groupe17.cashmanager.DAO.JpaRepositories.CartRespository;
import com.groupe17.cashmanager.DAO.JpaRepositories.ProductRespository;
import com.groupe17.cashmanager.DTO.Product;
import com.groupe17.cashmanager.DTO.plurals.Products;
import com.groupe17.cashmanager.configuration.Constants;
import com.groupe17.cashmanager.exception.ProductNotFoundException;
import io.swagger.annotations.Api;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.Optional;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@Api(tags = "Products")
public class ProductRoutes {
    private final ProductRespository productRespository;

    public ProductRoutes(ProductRespository productRespository) {
        this.productRespository = productRespository;
    }

    /***
     * Find All products
     * @return All products
     */
    @GetMapping("/products")
    public Resources<Product> findAll() {
        ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).findAll());
        Resources<Product> products =   new Resources<>(productRespository.findAll());
        products.add(linkTo.withRel("All methods"));
        return products;
    }

    /***
     * Find a product with his Id
     * @param id Id Of the product to find
     * @return Product
     */
    @GetMapping("/products/{id}")
    public Resource<Product> findOne(@PathVariable long id) {
        Optional<Product> product =  productRespository.findById(id);
        if (!product.isPresent()) {
            throw new ProductNotFoundException(Constants.MESSAGE_ERREUR_PRODUCT_NOT_FOUND);
        }
        Resource<Product> resource = new Resource<>(product.get());
        ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).findAll());
        resource.add(linkTo.withRel("All products"));
        return resource;
    }

    /***
     * Create a Product
     * @param product product to create
     * @return Product
     */
    @PostMapping("/products")
    public ResponseEntity<Resource<Product>> createProduct(@Valid @RequestBody Product product) {
        Product productInserted = productRespository.save(product);
        ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).findOne(productInserted.getId()));
        Resource<Product> resource = new Resource<>(productInserted);
        resource.add(linkTo.withRel(Constants.HATEOAS_SELF));
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(productInserted.getId()).toUri();
        return ResponseEntity.created(location).body(resource);
    }

    /***
     * Delete a product
     * @param id Id of the product to delete
     * @return Product
     */
    @DeleteMapping("/products/{id}")
    public ResponseEntity<Resource<Product>>  deleteProduct(@PathVariable long id) {
        Resource<Product> resource = this.findOne(id);
        System.out.println(resource.getContent().getId());
        resource.getLinks().clear();
        resource.add(linkTo(methodOn(this.getClass()).findAll()).withRel("All Product"));
        productRespository.deleteById(id);
        return ResponseEntity.accepted().body(resource);
    }

}
