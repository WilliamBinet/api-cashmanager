package com.groupe17.cashmanager.DTO;

import com.groupe17.cashmanager.configuration.JWTToken;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Terminal {
    private String id;
    private Cart cart;
    private String JWT_Token;
    private static List<Terminal> clients = new ArrayList<>();

    public Terminal(Cart cart) {
        this.id = UUID.randomUUID().toString();
        this.cart = cart;
        this.JWT_Token = new JWTToken(this).getToken();
        clients.add(this);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public String getJWT_Token() {
        return JWT_Token;
    }


    /***
     * Retrieve the Terminal linked to the JWT Token
     * @param jwtToken Jwt token used to retrieve the Terminal
     * @return Terminal
     */
    public static Terminal findTerminalViaJWTToken (String jwtToken){
        Jws<Claims> result = JWTToken.getJwsResult(jwtToken);
        return Terminal.findTerminalById((String) result.getBody().get("Terminal_ID"));
    }


    /***
     * Find terminal thanks to his UUIS
     * @param id UUID
     * @return Terminal
     */
    public static Terminal findTerminalById(String id) {
        for (Terminal client : clients) {
            if (client.getId().equals(id)){
                return client;
            }
        }
        return null;
    }
}