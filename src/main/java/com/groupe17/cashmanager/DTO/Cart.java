package com.groupe17.cashmanager.DTO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.groupe17.cashmanager.DTO.payment.paymentMethod.PaymentMethod;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;


import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Entity(name = "Cart")
@Embeddable
public class Cart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    long id;
    @Column(name = "total")
    Double total = 0D;
    @Transient
    @JsonIgnore
    Terminal terminal;
    @Transient
    @JsonIgnore
    PaymentMethod paymentMethod;

    @ElementCollection
    @Column(name = "Quantite")
    @CollectionTable(name = "LignePanier")
    @MapKeyJoinColumn(name = "Product")
    Map<Product, Integer> articles = new HashMap<>();

    public Map<Product, Integer> getArticles() {
        return articles;
    }

    public void setArticles(HashMap<Product, Integer> articles) {
        this.articles = articles;
    }

    public Cart() {
    }

    public Cart(long id, Double total, Terminal terminal, PaymentMethod paymentMethod) {
        this.id = id;
        this.total = total;
        this.terminal = terminal;
        this.paymentMethod = paymentMethod;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Terminal getTerminal() {
        return terminal;
    }

    public void setTerminal(Terminal terminal) {
        this.terminal = terminal;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    /***
     * Add a product to the cart
     * @param product Product to add
     */
    public void addProduct(Product product){
        Product productSingleton = productKeySingleton(product);
        if (articles.containsKey(productSingleton)){
            articles.put(productSingleton, articles.get(productSingleton) + 1);
        } else {
            articles.put(productSingleton, 1);
        }
        calculateTotal();
    }

    /***
     * Add a product to the cart
     * @param product Product to remove
     */
    public void removeProduct(Product product){
        Product productSingleton = productKeySingleton(product);
        if (this.getArticles().containsKey(productSingleton)){
            int newValue = articles.get(productSingleton) - 1;
            if (newValue == 0) {
                articles.remove(product);
            } else {
                articles.put(productSingleton, newValue);
            }
        }
        calculateTotal();
    }

    /***
     * Check if a product is already in the cart
     * @param product Product to check if it is already in the cart
     * @return Product
     */
    private Product productKeySingleton(Product product) {
        for (Product productIter : articles.keySet()) {
            if (productIter.getId() == product.getId()) {
                return productIter;
            }
        }
        return product;
    }


    /***
     * Calculate the total of the cart
     */
    private void calculateTotal(){
        this.total = 0D;
        for (Product product : articles.keySet()) {
            this.total += product.getPrice() * articles.get(product);
        }
        this.total = Math.round(this.total * 100.0) / 100.0;

    }


    public void clearAllProduct() {
        this.articles.clear();
        calculateTotal();
    }
}