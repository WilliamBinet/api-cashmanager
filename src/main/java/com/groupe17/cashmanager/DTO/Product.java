package com.groupe17.cashmanager.DTO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity(name = "Product")
@Embeddable
@ApiModel(description = "Correspond à un produit scannable")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    @Column(name = "name")
    private String name;
    @Column(name = "price")
    private Double price;

    @Size(min = 13, max = 13, message = "Reference should have a length of 13 characters")
    @ApiModelProperty(notes = "The reference should have a length of 13 characters")
    @Column(name = "reference")
    private String reference;

    public Product() {
    }

    public Product(int id, String name, Double price, String reference) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.reference = reference;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", reference='" + reference + '\'' +
                '}';
    }
}
