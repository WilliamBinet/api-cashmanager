package com.groupe17.cashmanager.DTO.plurals;

import com.groupe17.cashmanager.DTO.Cart;
import com.groupe17.cashmanager.configuration.ControllerLinks;
import org.springframework.hateoas.Resource;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Carts extends ArrayList<Cart> {

    public Carts(Collection<? extends Cart> c) {
        super(c);
    }

    /***
     * Convert the list of cart into a List of Resource Cart
     * @return List Resource
     */
    public List<Resource<Cart>> toResources(){
        List<Resource<Cart>> resources = new ArrayList<>();
        for (Cart cart : this) {
            resources.add(new Resource<>(cart, ControllerLinks.getCurrentLinkToCart(cart.getId())));
        }
        return resources;
    }
}
