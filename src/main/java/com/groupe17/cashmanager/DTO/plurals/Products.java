package com.groupe17.cashmanager.DTO.plurals;

import com.groupe17.cashmanager.DTO.Product;

import java.util.ArrayList;
import java.util.List;


public class Products extends ArrayList<Product> {
    String id;

    public Products(String id) {
        this.id = id;
    }

    public Products() {

    }

    public Products(List<Product> all) {
        this.addAll(all);
    }
}
