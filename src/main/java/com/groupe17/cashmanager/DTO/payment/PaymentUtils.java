package com.groupe17.cashmanager.DTO.payment;



import com.groupe17.cashmanager.DTO.payment.paymentMethod.Cheque;
import com.groupe17.cashmanager.DTO.payment.paymentMethod.CreditCard;
import com.groupe17.cashmanager.configuration.Constants;
import com.groupe17.cashmanager.exception.InvalidCreditCardException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PaymentUtils {

    /***
     * Regex to match for a valid CB
     */
    private static final Pattern regexCB = Pattern.compile("^(?:6011\\d{12})|(?:65\\d{14})$") ;


    /***
     * Check if the credit card is valid with the regex 'regexCB'
     * @param creditCard Credit card to check
     * @return Boolean
     */
    public static Boolean isCreditCardValid (CreditCard creditCard) {
        Matcher matcher = regexCB.matcher(creditCard.getCardNumber());
        return matcher.matches();
    }

    /***
     * Check if the cheque is valid with the regex 'none'
     * @param cheque Cheque to check
     * @return Boolean
     */
    public static Boolean isChequeValid(Cheque cheque) {
        //TODO check validity of cheque
        return  true;
    }


    /***
     * Increase the attempt number of the credit card
     * @param creditCard Credit card to increase the attempt number
     * @throws InvalidCreditCardException Max attempt reached
     */
    static void increaseAttemptCreditCart(CreditCard creditCard) throws InvalidCreditCardException {
        if (creditCard.getAttemptNumber() >= Constants.MAX_ATTEMPT_CB){
            throw new InvalidCreditCardException(Constants.MESSAGE_ERREUR_MAX_ATTEMPT_PAYMENT);
        }
        creditCard.setAttemptNumber( creditCard.getAttemptNumber() + 1);
    }

}
