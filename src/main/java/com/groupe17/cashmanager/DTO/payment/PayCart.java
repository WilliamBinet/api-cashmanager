package com.groupe17.cashmanager.DTO.payment;

import com.groupe17.cashmanager.DTO.Cart;
import com.groupe17.cashmanager.DTO.payment.paymentMethod.Cheque;
import com.groupe17.cashmanager.DTO.payment.paymentMethod.CreditCard;
import com.groupe17.cashmanager.DTO.payment.paymentMethod.PaymentMethod;
import com.groupe17.cashmanager.DTO.payment.paymentMethod.PaymentState;
import com.groupe17.cashmanager.configuration.Constants;
import com.groupe17.cashmanager.exception.InvalidCreditCardException;

public  class PayCart {


    /***
     * Pay the cart pass to param with a cheque
     * @param cart Cart to pay
     */
    private static void payCheque(Cart cart) {
        cart.getPaymentMethod().paymentState = PaymentState.ACCEPTED;
    }

    /***
     * Launch the procedure of payment of the cart based on his paymentMethod
     * @param cart Cart to Pay
     * @return The payment method of the cart
     */
    public static PaymentMethod pay(Cart cart) {
        if (cart.getPaymentMethod().paymentState != PaymentState.ACCEPTED){
            if (cart.getPaymentMethod().getClass() == Cheque.class){
                PayCart.payCheque(cart);
            } else if (cart.getPaymentMethod().getClass() == CreditCard.class) {
                PayCart.payCard(cart);
            }
        }
        return cart.getPaymentMethod();
    }

    /***
     * Pay the cart pass to param with a Credit Card
     * @param cart cart to pay
     */
    private static void payCard(Cart cart) {
        if (cart.getPaymentMethod().getClass() == CreditCard.class){
            CreditCard creditCard = (CreditCard) cart.getPaymentMethod();
            PaymentUtils.increaseAttemptCreditCart(creditCard);
            if (PaymentUtils.isCreditCardValid(creditCard)){
                cart.getPaymentMethod().paymentState = PaymentState.ACCEPTED;
            } else {
                cart.getPaymentMethod().paymentState = PaymentState.PENDING;
                throw new InvalidCreditCardException(Constants.MESSAGE_ERREUR_INVALID_CARD);
            }
        }
    }

    /***
     * Cancel the payment of the cart
     * @param cart Cart to cancel
     * @return Payment Method of the cart
     */
    public static PaymentMethod cancelPayment(Cart cart){
        if (cart.getPaymentMethod() == null) {
            cart.setPaymentMethod(new CreditCard(null, null, null));
        }
        cart.getPaymentMethod().paymentState = PaymentState.CANCELED;
        return cart.getPaymentMethod();
    }
}
