package com.groupe17.cashmanager.DTO.payment.paymentMethod;

public class Cheque extends PaymentMethod {
    private Double amount;
    private String order;
    private String chequeNumber;

    public Cheque(Double amount, String order, String chequeNumber) {
        this.amount = amount;
        this.order = order;
        this.chequeNumber = chequeNumber;
    }
}
