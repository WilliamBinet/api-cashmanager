package com.groupe17.cashmanager.DTO.payment.paymentMethod;

public abstract class PaymentMethod {
    protected String id;
    protected String ownerName;
    protected String ownerFirstName;
    public PaymentState paymentState;
    protected Integer attemptNumber = 0;
}



