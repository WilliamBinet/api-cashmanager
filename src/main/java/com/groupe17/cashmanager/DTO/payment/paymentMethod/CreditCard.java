package com.groupe17.cashmanager.DTO.payment.paymentMethod;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class CreditCard extends PaymentMethod {
    @NotNull(message = "Vous devez précisez un numéro de carte")
    private
    String cardNumber;
    @NotNull(message = "Vous devez précisez une date de validité")
    private
    Date validityDate;
    @NotNull(message = "Vous devez précisez un code de sécurité")
    @NotNull
    private
    String securityCode;

    public CreditCard(String cardNumber, Date validityDate, String securityCode) {
        this.cardNumber = cardNumber;
        this.validityDate = validityDate;
        this.securityCode = securityCode;
    }

    /***
     * Check if the object obj is a credit cart with the same card number
     * @param obj Object to Compare
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() == CreditCard.class){
            CreditCard creditCardToCheck = (CreditCard) obj;
            return this.cardNumber.equals(creditCardToCheck.cardNumber);
        }
        return false;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Date getValidityDate() {
        return validityDate;
    }

    public void setValidityDate(Date validityDate) {
        this.validityDate = validityDate;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    public Integer getAttemptNumber() {
        return this.attemptNumber;
    }

    public void setAttemptNumber(Integer attemptNumber) {
        this.attemptNumber = attemptNumber;
    }

}
