package com.groupe17.cashmanager.DTO.payment.paymentMethod;

public enum PaymentState {
    NOT_PAID,
    PENDING,
    ACCEPTED,
    REFUSED,
    CANCELED
}
