package com.groupe17.cashmanager.routes;

import com.groupe17.cashmanager.DAO.JpaRepositories.CartRespository;
import com.groupe17.cashmanager.DAO.JpaRepositories.ProductRespository;
import com.groupe17.cashmanager.DTO.Cart;
import com.groupe17.cashmanager.DTO.Product;
import com.groupe17.cashmanager.DTO.Terminal;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class CartRoutesTest {

    @Mock
    private CartRespository mockCartRespository;
    @Mock
    private ProductRespository mockProductRespository;

    MockHttpServletRequest request = new MockHttpServletRequest();

    @InjectMocks
    private CartRoutes cartRoutesUnderTest;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void testFindAll() {
        // Setup
        final Resources<Resource<Cart>> expectedResult = new Resources<>(Arrays.asList(), Arrays.asList());
        when(mockCartRespository.findAll()).thenReturn(Arrays.asList());

        // Run the test
        final Resources<Resource<Cart>> result = cartRoutesUnderTest.findAll();

        // Verify the results
        assertEquals(expectedResult, result);
    }

    @Test
    public void testFindOne() {
        // Setup
        Cart cart = new Cart();
        cart.setId(0L);
        final Resource<Cart> expectedResult = new Resource<>(cart);
        when(mockCartRespository.findById(0L)).thenReturn(Optional.of(cart));
        // Run the test
        final Resource<Cart> result = cartRoutesUnderTest.findOne(0L);

        // Verify the results
        assertEquals(expectedResult.getContent(), result.getContent());
    }

    @Test
    public void testAddToCart() {
        // Setup
        final Map<String, String> reference = new HashMap<>();
        reference.put("reference", "reference");
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        Cart cart = new Cart();
        Terminal terminal = new Terminal(cart);
        cart.setId(0L);
        Product product = new Product(0, "Coca", 1D, "1234567890");
        final ResponseEntity<Resource<Cart>> expectedResult = new ResponseEntity<>(HttpStatus.CREATED);
        when(mockCartRespository.findById(0L)).thenReturn(Optional.of(cart));
        when(mockProductRespository.findProductByReference("reference")).thenReturn(Optional.of(product));
        when(mockCartRespository.save(any(Cart.class))).thenReturn(cart);

        // Run the test
        final ResponseEntity<Resource<Cart>> result = cartRoutesUnderTest.addToCart(0L, reference, terminal.getJWT_Token());

        // Verify the results
        assertEquals(expectedResult.getStatusCode(), result.getStatusCode());
    }

    @Test
    public void testRemoveFromCart() {
        // Setup
        final Map<String, String> reference = new HashMap<>();
        reference.put("reference", "reference");
        Cart cart = new Cart();
        Terminal terminal = new Terminal(cart);
        cart.setId(0L);
        Product product = new Product(0, "Coca", 1D, "1234567890");

        final ResponseEntity<Resource<Cart>> expectedResult = new ResponseEntity<>( HttpStatus.ACCEPTED);
        when(mockCartRespository.findById(0L)).thenReturn(Optional.of(cart));
        when(mockProductRespository.findProductByReference("reference")).thenReturn(Optional.of(product));
        when(mockCartRespository.save(any(Cart.class))).thenReturn(cart);

        // Run the test
        final ResponseEntity<Resource<Cart>> result = cartRoutesUnderTest.removeFromCart(0L, reference);
        // Verify the results
        assertEquals(expectedResult.getStatusCode(), result.getStatusCode());
    }

    @Test
    public void testCleanCart() {
        // Setup
        final ResponseEntity<Resource<Cart>> expectedResult = new ResponseEntity<>(HttpStatus.ACCEPTED);
        Cart cart = new Cart();
        Terminal terminal = new Terminal(cart);
        cart.setId(0L);
        Product product = new Product(0, "Coca", 1D, "1234567890");

        when(mockCartRespository.findById(0L)).thenReturn(Optional.of(cart));
        when(mockCartRespository.save(any(Cart.class))).thenReturn(cart);

        // Run the test
        final ResponseEntity<Resource<Cart>> result = cartRoutesUnderTest.cleanCart(0L);

        // Verify the results
        assertEquals(expectedResult.getStatusCode(), result.getStatusCode());
    }
}
