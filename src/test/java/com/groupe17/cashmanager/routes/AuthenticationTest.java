package com.groupe17.cashmanager.routes;

import com.groupe17.cashmanager.DAO.JpaRepositories.CartRespository;
import com.groupe17.cashmanager.DTO.Cart;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.hateoas.Resource;

import java.util.HashMap;
import java.util.Map;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class AuthenticationTest {

    @Mock
    public CartRespository mockCartRespository;

    public Authentication authenticationUnderTest;

    @Before
    public void setUp() {
        initMocks(this);
        authenticationUnderTest = new Authentication(mockCartRespository);
    }

    @Test
    public void testCreateNewClient() {
        // Setup
        final Map<String, String> password = new HashMap<>();
        password.put("password", "password");
        when(mockCartRespository.save(any(Cart.class))).thenReturn(new Cart(0L, 0.0, null, null));

        // Run the test
        final Resource<Map<String, String>> result = authenticationUnderTest.createNewClient(password);

        // Verify the results
        assertNotNull(result);
    }
}
