package com.groupe17.cashmanager.routes;

import com.groupe17.cashmanager.DAO.JpaRepositories.ProductRespository;
import com.groupe17.cashmanager.DTO.Product;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class ProductRoutesTest {

    @Mock
    private ProductRespository mockProductRespository;

    private ProductRoutes productRoutesUnderTest;

    MockHttpServletRequest request = new MockHttpServletRequest();

    @Before
    public void setUp() {
        initMocks(this);
        productRoutesUnderTest = new ProductRoutes(mockProductRespository);
    }

    @Test
    public void testFindAll() {
        // Setup
        final Resources<Product> expectedResult = new Resources<>(Arrays.asList(), Arrays.asList());
        when(mockProductRespository.findAll()).thenReturn(Arrays.asList());

        // Run the test
        final Resources<Product> result = productRoutesUnderTest.findAll();

        // Verify the results
        assertEquals(expectedResult.getContent().size(), result.getContent().size());
    }

    @Test
    public void testFindOne() {
        // Setup
        Product product = new Product(0,"Coca", 2D, "1234567890");
        final Resource<Product> expectedResult = new Resource<>(product);
        when(mockProductRespository.findById(0L)).thenReturn(Optional.of(product));
        when(mockProductRespository.findAll()).thenReturn(Arrays.asList(product));

        // Run the test
        final Resource<Product> result = productRoutesUnderTest.findOne(0L);

        // Verify the results
        assertEquals(expectedResult.getContent(), result.getContent());
    }

    @Test
    public void testCreateProduct() {
        // Setup
        final Product product = new Product(0, "name", 0.0, "reference");
        final ResponseEntity<Resource<Product>> expectedResult = new ResponseEntity<>(HttpStatus.CREATED);
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));

        when(mockProductRespository.save(any(Product.class))).thenReturn(product);
        when(mockProductRespository.findById(0L)).thenReturn(Optional.of(product));
        when(mockProductRespository.findAll()).thenReturn(Arrays.asList(product));

        // Run the test
        final ResponseEntity<Resource<Product>> result = productRoutesUnderTest.createProduct(product);

        // Verify the results
        assertEquals(expectedResult.getStatusCode(), result.getStatusCode());
    }

    @Test
    public void testDeleteProduct() {
        // Setup
        final Product product = new Product(0, "name", 0.0, "reference");
        final ResponseEntity<Resource<Product>> expectedResult = new ResponseEntity<>(HttpStatus.ACCEPTED);
        when(mockProductRespository.findById(0L)).thenReturn(Optional.of(product));
        when(mockProductRespository.findAll()).thenReturn(Arrays.asList(product));

        // Run the test
        final ResponseEntity<Resource<Product>> result = productRoutesUnderTest.deleteProduct(0L);

        // Verify the results
        assertEquals(expectedResult.getStatusCode(), result.getStatusCode());
    }
}
