package com.groupe17.cashmanager.routes;

import com.groupe17.cashmanager.DAO.JpaRepositories.CartRespository;
import com.groupe17.cashmanager.DTO.Cart;
import com.groupe17.cashmanager.DTO.Product;
import com.groupe17.cashmanager.DTO.Terminal;
import com.groupe17.cashmanager.DTO.payment.paymentMethod.Cheque;
import com.groupe17.cashmanager.DTO.payment.paymentMethod.CreditCard;
import com.groupe17.cashmanager.DTO.payment.paymentMethod.PaymentState;
import com.groupe17.cashmanager.exception.InvalidCreditCardException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.hateoas.Resource;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.calls;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class PaymentRoutesTest {

    @Mock
    private CartRespository mockCartRespository;

    private PaymentRoutes paymentRoutesUnderTest;

    @Before
    public void setUp() {
        initMocks(this);
        paymentRoutesUnderTest = new PaymentRoutes(mockCartRespository);
    }

    @Test
    public void testPayCBOK() {
        // Setup
        final CreditCard creditCard = new CreditCard("6011000000000000", new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime(), "234");
        final Resource<PaymentState> expectedResult = new Resource<>(PaymentState.ACCEPTED);

        Cart cart = new Cart();
        Terminal terminal = new Terminal(cart);
        cart.setId(0L);
        // Run the test
        final Resource<PaymentState> result = paymentRoutesUnderTest.pay(creditCard, 0L, terminal.getJWT_Token());

        // Verify the results
        assertEquals(expectedResult.getContent(), result.getContent());
    }


    @Test
    public void testPayCBNOK() {
        // Setup
        final CreditCard creditCard = new CreditCard("3421000000000000", new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime(), "234");
        final InvalidCreditCardException expectedResult = new InvalidCreditCardException("La carte est invalide");
        try {
            Cart cart = new Cart();
            Terminal terminal = new Terminal(cart);
            cart.setId(0L);
            // Run the test
            final Resource<PaymentState> result = paymentRoutesUnderTest.pay(creditCard, 0L, terminal.getJWT_Token());
        } catch (InvalidCreditCardException e) {
            assertEquals(expectedResult.getMessage(), e.getMessage());
        }
    }

    @Test
    public void testPayByCheque() {
        // Setup
        final Cheque cheque = new Cheque(0.0, "order", "chequeNumber");
        final Resource<PaymentState> expectedResult = new Resource<>(PaymentState.ACCEPTED);
        Cart cart = new Cart();
        Terminal terminal = new Terminal(cart);
        cart.setId(0L);
        when(mockCartRespository.findById(0L)).thenReturn(Optional.of(cart));

        // Run the test
        final Resource<PaymentState> result = paymentRoutesUnderTest.pay(cheque, 0L, terminal.getJWT_Token());

        // Verify the results
        assertEquals(expectedResult.getContent(), result.getContent());
    }

    @Test
    public void testCancelPayment() {
        // Setup
        Cart cart = new Cart();
        Terminal terminal = new Terminal(cart);
        cart.setId(0L);


        final Resource<PaymentState> expectedResult = new Resource<>(PaymentState.CANCELED);
        when(mockCartRespository.findById(0L)).thenReturn(Optional.of(cart));

        // Run the test
        final Resource<PaymentState> result = paymentRoutesUnderTest.cancelPayment(0L, terminal.getJWT_Token());

        // Verify the results
        assertEquals(expectedResult.getContent(), result.getContent());
    }
}
