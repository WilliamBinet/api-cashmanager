package com.groupe17.cashmanager.configuration;

import com.groupe17.cashmanager.DAO.JpaRepositories.CartRespository;
import com.groupe17.cashmanager.DTO.Cart;
import com.groupe17.cashmanager.DTO.Terminal;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertTrue;


public class JWTTokenTest {
    @Autowired
    CartRespository cartRespository;
    private Cart cart;
    private Terminal terminal;

    @Before
    public void before() {
        this.cart = new Cart(1, 2D, null, null);
        this.terminal = new Terminal(cart);
    }

    @Test
    public void isSameCart() {
        System.out.println("terminal = " + terminal);
        System.out.println("terminal.getJWT_Token() = " + terminal.getJWT_Token());
        assertTrue(JWTToken.isSameCart(terminal.getJWT_Token(), cart.getId()));
    }


    @Test
    public void isJWTValid() {
        assertTrue(JWTToken.isJWTValid(terminal.getJWT_Token()));
    }
}