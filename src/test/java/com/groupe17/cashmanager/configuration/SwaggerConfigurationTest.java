package com.groupe17.cashmanager.configuration;


import org.junit.Test;
import springfox.documentation.spring.web.plugins.Docket;

import static org.junit.Assert.assertNotEquals;

public class SwaggerConfigurationTest {

    @Test
    public void api() {
        SwaggerConfiguration swaggerConfiguration = new SwaggerConfiguration();
        assertNotEquals(null, swaggerConfiguration.api());
    }

}