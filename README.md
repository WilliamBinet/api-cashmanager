# Cash Manager API

## Participants

* HEYMANS Arnaud
* LEROY Romain
* BINET William

## Objective

	This project is an API whitch allow a cashmanager to proceed to a complete checkout of an order

## Getting started
    
# Build the project
	java -jar target/cashmanager-0.0.1-SNAPSHOT.jar
	
#Useful links
* Access to the database: http://localhost:8080/h2-console/
* Access to the swagger: http://localhost:8080/swagger-ui.html
